"""Game world & environment."""

from pyglet import gl  # type: ignore[import]

from gfolf import res

_MAP_SIZE = 256
_WATER = 1
_LAND = 0


class World:
    def __init__(self) -> None:
        self.terrain = res.atlas.GFOLF_ZONE
        self.water_map = res.atlas.WATER_MAP

    def render(self) -> None:
        terrain_tex = self.terrain.get_texture()
        gl.glEnable(terrain_tex.target)
        gl.glBindTexture(terrain_tex.target, terrain_tex.id)
        gl.glTexParameteri(terrain_tex.target, gl.GL_TEXTURE_MAG_FILTER, gl.GL_NEAREST)
        gl.glBegin(gl.GL_TRIANGLE_FAN)
        gl.glTexCoord2f(0.0, 5.0)
        gl.glVertex3f(0, 0, 0)
        gl.glTexCoord2f(5.0, 5.0)
        gl.glVertex3f(5 * _MAP_SIZE, 0, 0)
        gl.glTexCoord2f(5.0, 0.0)
        gl.glVertex3f(5 * _MAP_SIZE, 0, 5 * _MAP_SIZE)
        gl.glTexCoord2f(0.0, 0.0)
        gl.glVertex3f(0, 0, 5 * _MAP_SIZE)
        gl.glEnd()
        gl.glDisable(terrain_tex.target)

    def on_land(self, x: float, y: float) -> bool:
        lin_map = (int(y) % _MAP_SIZE) * _MAP_SIZE + (int(x) % _MAP_SIZE)
        return self.water_map[lin_map] == _LAND
