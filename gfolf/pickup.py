"""Consumable items in the game world."""
import typing as T

from pyglet import gl  # type: ignore[import]

from gfolf import game, res
from gfolf.blinker import Blinker

if T.TYPE_CHECKING:
    import pyglet
    from gfolf.player import Player
    from gfolf.game import Game


class Pickup:
    def __init__(
        self, sprite: "pyglet.image.Texture", pos: tuple[float, float] = (0, 0)
    ) -> None:
        self.sprite = sprite
        self.active = True
        self.pos = pos

    def render(self) -> None:
        tex = self.sprite.get_texture()
        gl.glEnable(tex.target)
        gl.glBindTexture(tex.target, tex.id)
        gl.glTexParameteri(tex.target, gl.GL_TEXTURE_MAG_FILTER, gl.GL_NEAREST)
        gl.glTexParameteri(tex.target, gl.GL_TEXTURE_MIN_FILTER, gl.GL_LINEAR)
        gl.glBegin(gl.GL_TRIANGLE_FAN)
        gl.glTexCoord2f(0.0, 0.0)
        gl.glVertex3f(-1, 0.0, 0)
        gl.glTexCoord2f(0.0, 1.0)
        gl.glVertex3f(-1, 2.0, 0)
        gl.glTexCoord2f(1.0, 1.0)
        gl.glVertex3f(1, 2.0, 0)
        gl.glTexCoord2f(1.0, 0.0)
        gl.glVertex3f(1, 0.0, 0)
        gl.glEnd()
        gl.glDisable(tex.target)

    def check_collision(self, player: "Player") -> None:
        if self.active and game.distance(self.pos, player.pos) <= 1.5:
            self.active = False
            self.pick_up(player)

    def pick_up(self, player: "Player") -> None:
        pass

    def despawn(self, game: "Game") -> None:
        pass


class Stapler(Pickup):
    def __init__(self, pos: tuple[float, float] = (0, 0)) -> None:
        super(Stapler, self).__init__(res.atlas.STAPLER, pos)

    def pick_up(self, player: "Player") -> None:
        Blinker.start_message("NICE FUCKIN' STAPLER", 3)
        self.collector = player

    def despawn(self, game: "Game") -> None:
        game.double_turn = True
