"""GFOLF application entrypoint."""

from gfolf.game import Game


def main() -> None:
    g = Game()
    g.run()


if __name__ == "__main__":
    main()
