"""Game camera logic."""
import typing as T

from pyglet import gl  # type: ignore[import]

from gfolf import res
from gfolf.blinker import Blinker

if T.TYPE_CHECKING:
    import pyglet
    from gfolf.pickup import Pickup
    from gfolf.player import Player
    from gfolf.world import World


_CAMERA_HEIGHT = 1.5
_CAMERA_DIST = 4.3
_PITCH_LIMITS = (0, 25)


def ortho_start(dim: tuple[int, int]) -> None:
    gl.glMatrixMode(gl.GL_PROJECTION)
    gl.glPushMatrix()
    gl.glLoadIdentity()
    gl.gluOrtho2D(0, dim[0], 0, dim[1])
    gl.glMatrixMode(gl.GL_MODELVIEW)


def ortho_end(dim: tuple[int, int]) -> None:
    gl.glMatrixMode(gl.GL_PROJECTION)
    gl.glPopMatrix()
    gl.glMatrixMode(gl.GL_MODELVIEW)


def gl_init() -> None:
    gl.glEnable(gl.GL_BLEND)
    gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)


def tuple_diff(A: tuple[float, float], B: tuple[float, float]) -> tuple[float, float]:
    return (A[0] - B[0], A[1] - B[1])


class Camera:
    def __init__(self, width: int, height: int) -> None:
        self.background = res.atlas.VISTA_BG
        self.rpy: tuple[float, float, float] = (0, 2, 0)
        self.refresh(width, height)
        self.dimension = (width, height)

        gl_init()

    def translate(self, pos: tuple[float, float]) -> None:
        gl.glTranslatef(-pos[0], -_CAMERA_HEIGHT, -pos[1])

    def rotate(self) -> None:
        gl.glTranslatef(0, 0, -_CAMERA_DIST)

        gl.glRotatef(self.rpy[1], 1, 0, 0)
        gl.glRotatef(self.rpy[2], 0, 1, 0)
        gl.glRotatef(self.rpy[0], 0, 0, 1)

    def billboard_rotate(self) -> None:
        gl.glRotatef(self.rpy[2], 0, -1, 0)

    def routine(
        self,
        world: "World",
        main_player: "Player",
        other_players: list["Player"],
        items: list["Pickup"],
    ) -> None:
        gl.glClear(gl.GL_COLOR_BUFFER_BIT)

        self.draw_bg()

        gl.glPushMatrix()
        gl.glLoadIdentity()

        self.rotate()

        self.translate(main_player.pos)

        world.render()

        gl.glPopMatrix()

        gl.glPushMatrix()
        gl.glLoadIdentity()
        self.rotate()
        for p in other_players:
            gl.glPushMatrix()
            self.translate(tuple_diff(main_player.pos, p.pos))
            self.billboard_rotate()
            p.bg_render()
            gl.glPopMatrix()
        for i in items:
            gl.glPushMatrix()
            self.translate(tuple_diff(main_player.pos, i.pos))
            self.billboard_rotate()
            i.render()
            gl.glPopMatrix()
        gl.glPopMatrix()

        self.draw_main_player(main_player)

    def draw_bg(self) -> None:
        ortho_start(self.dimension)

        self.background.blit(-1800 + (360 - self.rpy[2]) * 5, self.rpy[1] * 8)

        ortho_end(self.dimension)

    def draw_main_player(self, main_player: "Player") -> None:
        ortho_start(self.dimension)

        main_player.main_render(self.dimension[0], self.dimension[1])

        Blinker.try_draw()

        ortho_end(self.dimension)

    def draw_billboard(
        self, image: "pyglet.image.Texture", pos: tuple[float, float]
    ) -> None:
        gl.glPushMatrix()
        gl.glRotatef(self.rpy[0], 0, 0, 1)
        self.translate(pos)

        image.blit(0, 0)

        gl.glPopMatrix()

    def refresh(self, width: int, height: int) -> None:
        gl.glViewport(0, 0, width, height)
        gl.glMatrixMode(gl.GL_PROJECTION)
        gl.glLoadIdentity()
        gl.gluPerspective(65, width / float(height), 0.1, 1000)
        gl.glMatrixMode(gl.GL_MODELVIEW)

        self.dimension = (width, height)

    def move(self, dRpy: tuple[float, float, float]) -> None:
        new_pitch = self.rpy[1] - dRpy[1]
        if new_pitch < _PITCH_LIMITS[0] or new_pitch > _PITCH_LIMITS[1]:
            dRpy = (dRpy[0], 0, dRpy[2])
        self.rpy = (
            (self.rpy[0] - dRpy[0]) % 360,
            (self.rpy[1] - dRpy[1]) % 360,
            (self.rpy[2] - dRpy[2]) % 360,
        )

    def set(self, newRpy: tuple[float, float, float]) -> None:
        self.rpy = newRpy
