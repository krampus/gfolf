"""Game control bindings."""
import typing as T
from enum import Enum

from pyglet.window import key  # type: ignore[import]


class Button(Enum):
    LEFT = 1
    RIGHT = 2
    UP = 3
    DOWN = 4
    MENU = 5
    ACTION = 6


_BUTTON_MAP = {
    key.LEFT: Button.LEFT,
    key.RIGHT: Button.RIGHT,
    key.UP: Button.UP,
    key.DOWN: Button.DOWN,
    key.ESCAPE: Button.MENU,
    key.SPACE: Button.ACTION,
}

# Stateless buttons don't "repeat fire" when held down
_STATELESS_BUTTONS = (Button.MENU, Button.ACTION)


class GameController:
    def __init__(self) -> None:
        self.state = {b: False for b in Button}
        self.function: dict[Button, T.Callable[[], None] | None] = {}

    def press(self, key: int) -> None:
        if key in _BUTTON_MAP:
            button = _BUTTON_MAP[key]
            if button in _STATELESS_BUTTONS:
                self.fire(button)
            else:
                self.state[button] = True

    def release(self, key: int) -> None:
        if key in _BUTTON_MAP:
            button = _BUTTON_MAP[key]
            self.state[button] = False

    def get_state(self, button: Button) -> bool | None:
        return self.state.get(button, None)

    def hook_function(
        self, button: Button, function: T.Callable[[], None] | None
    ) -> None:
        self.function[button] = function

    def fire(self, button: Button) -> None:
        func = self.function[button]
        if func:
            func()

    def fire_all(self) -> None:
        for button in [b for b in self.state if self.state[b]]:
            self.fire(button)
