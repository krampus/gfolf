"""GFOLF, a game for two players."""

try:
    from gfolf._version import version as __version__
except ImportError:
    __version__ = "unknown"

from gfolf.__main__ import main  # noqa: F401
