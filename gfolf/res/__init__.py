"""Game resources."""
from functools import cached_property
from importlib import resources as imp_resources

import pyglet  # type: ignore[import]

RESOURCE_PATH = imp_resources.files(__package__)


def _load_image(filename: str) -> pyglet.image.Texture:
    res_file = RESOURCE_PATH / filename
    with res_file.open("rb") as fdat:
        return pyglet.image.load(filename, file=fdat)


def _gfolfer_sheet(filename: str) -> pyglet.image.ImageGrid:
    raw_sprite = _load_image(filename)
    return pyglet.image.ImageGrid(raw_sprite, 1, 9)


class _ResourceAtlas:
    @cached_property
    def WATER_MAP(self) -> bytearray:
        res_file = RESOURCE_PATH / "gfolf_water.data"
        with res_file.open("rb") as fdat:
            return bytearray(fdat.read())

    @cached_property
    def GFOLF_ZONE(self) -> pyglet.image.Texture:
        return _load_image("gfolfzone.png")

    @cached_property
    def VISTA_BG(self) -> pyglet.image.Texture:
        return _load_image("vista_bg.png")

    @cached_property
    def STAPLER(self) -> pyglet.image.Texture:
        return _load_image("stapler.png")

    @cached_property
    def GFOLF_BALL(self) -> pyglet.image.Texture:
        return _load_image("gfolfball.png")

    @cached_property
    def GFOLFER(self) -> pyglet.image.ImageGrid:
        return _gfolfer_sheet("gfolfer.png")

    @cached_property
    def GFOLFER_CYAN(self) -> pyglet.image.ImageGrid:
        return _gfolfer_sheet("gfolfer_cyan.png")

    @cached_property
    def GFOLFER_ORANGE(self) -> pyglet.image.ImageGrid:
        return _gfolfer_sheet("gfolfer_orange.png")

    @cached_property
    def DOPE_EXPLOSION(self) -> pyglet.image.Animation:
        res_file = RESOURCE_PATH / "dope_explosion.gif"
        with res_file.open("rb") as fdat:
            return pyglet.image.load_animation("dope_explosion.gif", file=fdat)


atlas = _ResourceAtlas()
