"""Game player character."""

from enum import Enum
from math import cos, radians, sin

from pyglet import clock, gl, sprite, text  # type: ignore[import]

from gfolf import game, res
from gfolf.blinker import Blinker

_FRAME_DELAY = 4

_CHARACTER_DIM = (94, 170)


class PlayerState(Enum):
    AIMING = 1
    ADJUSTING = 2
    POWERING = 3
    SHOOTING = 4
    WATCHING = 5
    EXPLODING = 6
    PAUSED = 7
    POST = 8


_ANIM_STATE = {
    PlayerState.AIMING: 0,
    PlayerState.ADJUSTING: 3,
    PlayerState.POWERING: 3,
    PlayerState.SHOOTING: 8,
    PlayerState.WATCHING: 0,
    PlayerState.POST: 0,
    PlayerState.EXPLODING: 0,
}


class PlayerColor(Enum):
    DEFAULT = "gray"
    CYAN = "cyan"
    ORANGE = "orange"


class Player:
    def __init__(
        self,
        pos: tuple[float, float] = (128, 128),
        color: PlayerColor = PlayerColor.DEFAULT,
    ) -> None:
        if color == PlayerColor.CYAN:
            self.spritesheet = res.atlas.GFOLFER_CYAN
        elif color == PlayerColor.ORANGE:
            self.spritesheet = res.atlas.GFOLFER_ORANGE
        else:
            self.spritesheet = res.atlas.GFOLFER

        self.explosion_sprite = sprite.Sprite(res.atlas.DOPE_EXPLOSION)

        self.color = color

        self.shot = Shot(pos)
        self.state = PlayerState.AIMING
        self.anim_i: int = 0
        self.frame_delay: int = 0

        self.pos = pos
        self.health: float = 100

        self.health_ui = text.Label(
            "",
            font_name="Andale Mono",
            font_size=18,
            color=(238, 19, 19, 255),
            x=20,
            y=20,
            anchor_x="left",
            anchor_y="center",
        )

    def main_render(self, width: int, height: int) -> None:
        # Render as main character
        cx = width / 2 - _CHARACTER_DIM[0]
        cy = height / 2 - _CHARACTER_DIM[1]

        if self.state in (PlayerState.EXPLODING, PlayerState.POST):
            self.explosion_sprite.position = (cx + 50, cy - 50)
            self.explosion_sprite.draw()
        elif self.state != PlayerState.WATCHING:
            self.spritesheet[self.anim_i].blit(cx, cy)
            self.ui_render()
            self.shot.render(cx, cy)
        else:
            self.shot.render(cx, cy)

        if self.frame_delay == 0:
            if self.anim_i != _ANIM_STATE[self.state]:
                self.anim_i = (self.anim_i + 1) % len(self.spritesheet)
                self.frame_delay = _FRAME_DELAY
        else:
            self.frame_delay -= 1

    def update(self, dt: float, other_players: list["Player"]) -> None:
        if self.state == PlayerState.WATCHING:
            self.pos = self.shot.compute_shot(dt)
            self.check_collisions(other_players)

    def bg_render(self) -> None:
        # Render as background character
        if self.state == PlayerState.EXPLODING:
            # TODO: This is a nightmare
            self.explosion_sprite.draw()
            i = self.explosion_sprite._frame_index
            tex = self.explosion_sprite.image.frames[i].image.get_texture()
        else:
            tex = self.spritesheet[0].get_texture()

        gl.glEnable(tex.target)
        gl.glBindTexture(tex.target, tex.id)
        gl.glTexParameteri(tex.target, gl.GL_TEXTURE_MAG_FILTER, gl.GL_NEAREST)
        gl.glTexParameteri(tex.target, gl.GL_TEXTURE_MIN_FILTER, gl.GL_LINEAR)
        gl.glBegin(gl.GL_TRIANGLE_FAN)
        gl.glTexCoord2f(0.0, 0.0)
        gl.glVertex3f(-1.0, 0, 0)
        gl.glTexCoord2f(0.0, 1.0)
        gl.glVertex3f(-1.0, 3.0, 0)
        gl.glTexCoord2f(1.0, 1.0)
        gl.glVertex3f(2.0, 3.0, 0)
        gl.glTexCoord2f(1.0, 0.0)
        gl.glVertex3f(2.0, 0, 0)
        gl.glEnd()
        gl.glDisable(tex.target)

    def ui_render(self) -> None:
        self.health_ui.text = "<3 " + str(self.health) + "%"
        self.health_ui.draw()

        if self.state == PlayerState.ADJUSTING:
            self.shot.build_adjust_label().draw()
        elif self.state == PlayerState.POWERING:
            self.shot.build_power_label().draw()

    def is_dead(self) -> bool:
        return self.health <= 0

    def start_aiming(self) -> None:
        self.state = PlayerState.AIMING

    def start_adjusting(self, angle: float) -> None:
        self.state = PlayerState.ADJUSTING
        self.shot = Shot(self.pos, angle)
        clock.schedule_interval(self.shot.inc_adjust, Shot.ADJUST_PERIOD)

    def start_powering(self) -> None:
        self.state = PlayerState.POWERING
        clock.unschedule(self.shot.inc_adjust)
        clock.schedule_interval(self.shot.inc_power, Shot.POWER_PERIOD)

    def start_shot(self) -> None:
        self.state = PlayerState.SHOOTING
        clock.unschedule(self.shot.inc_power)

    def watch_shot(self) -> None:
        self.state = PlayerState.WATCHING

    def start_exploding(self, damage: float) -> None:
        if self.state != PlayerState.EXPLODING:
            Blinker.start_message("GOT EM", 3)
            self.health -= damage
            self.state = PlayerState.EXPLODING

    def end_turn(self, on_land: bool) -> None:
        self.state = PlayerState.POST
        if on_land:
            Blinker.start_message("NNNICE", 1)
            self.pos = self.shot.compute_endpt()
        else:
            Blinker.start_message("WASTED", 1, color=Blinker._PALETTE["blue"])
            self.pos = self.shot.compute_endpt()
            clock.schedule_once(self.revert_shot, 1.6)
            self.health = self.health // 2 + 1

    def revert_shot(self, dt: float = 0) -> None:
        self.pos = self.shot.origin

    def check_collisions(self, other_players: list["Player"]) -> None:
        # TODO: Just... ugh
        for p in other_players:
            if game.distance(self.pos, p.pos) <= 1:
                p.start_exploding(self.shot.curved_power() // 2 + 1)


class Shot:
    MAX_ADJUST = 31
    ADJUST_PERIOD = 1 / 50
    MAX_POWER = 30
    POWER_PERIOD = 1 / 100
    TRAVEL_SECONDS = 5
    TRAVEL_TIME = 60 * TRAVEL_SECONDS

    def __init__(self, origin: tuple[float, float], angle: float = 0) -> None:
        self.sprite = res.atlas.GFOLF_BALL

        self.origin = origin
        self.angle = angle
        self.adjustment = 10
        self.power = 5
        self.increment = 1
        self.travel_i: float = 0

        self.meter = text.Label(
            "",
            font_name="Andale Mono",
            font_size=18,
            x=600,
            y=100,
            anchor_x="center",
            anchor_y="center",
        )

    def set_angle(self, angle: float) -> None:
        self.angle = angle

    def inc_adjust(self, dt: float = 0) -> None:
        if self.adjustment >= Shot.MAX_ADJUST or self.adjustment <= 0:
            self.increment *= -1
        self.adjustment += self.increment

    def inc_power(self, dt: float = 0) -> None:
        if self.power >= Shot.MAX_POWER or self.power <= 0:
            self.increment *= -1
        self.power += self.increment

    def build_adjust_label(self) -> text.Label:
        s = (
            "["
            + " " * (self.adjustment)
            + "I"
            + " " * (Shot.MAX_ADJUST - self.adjustment)
            + "]"
        )
        self.meter.text = s
        return self.meter

    def build_power_label(self) -> text.Label:
        s = "[" + ">" * (self.power) + " " * (Shot.MAX_POWER - self.power) + "]"
        self.meter.text = s
        return self.meter

    def compute_shot(self, dt: float) -> tuple[float, float]:
        # print('dt: ' + str(dt))
        shot_angle = self.angle + 4 * (self.adjustment - 15)
        amp = self.curved_power() * (self.travel_i / Shot.TRAVEL_SECONDS)
        endpoint = (
            self.origin[0] + sin(radians(shot_angle)) * amp,
            self.origin[1] - cos(radians(shot_angle)) * amp,
        )
        self.travel_i += dt
        # self.travel_i += 1
        return endpoint

    def compute_endpt(self) -> tuple[float, float]:
        self.travel_i = Shot.TRAVEL_SECONDS
        return self.compute_shot(0)

    def curved_power(self) -> float:
        return float(((self.power + 5) / 3) ** 2)

    def render(self, cx: float, cy: float) -> None:
        dy = cy * (2 - (self.travel_i / (Shot.TRAVEL_SECONDS / 2) - 1) ** 2) + 5
        self.sprite.blit(cx + 85, dy)
