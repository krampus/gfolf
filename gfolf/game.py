"""Game logic runtime."""
import typing as T

from math import sqrt
from random import randrange

from pyglet import app, clock, window  # type: ignore[import]

from gfolf.blinker import Blinker
from gfolf.camera import Camera
from gfolf.controller import Button, GameController
from gfolf.pickup import Stapler
from gfolf.player import Player, PlayerColor, Shot
from gfolf.world import World

if T.TYPE_CHECKING:
    from gfolf.pickup import Pickup

_FRAME_RATE = 1 / 60
_MOVE_SPEED = 2


def distance(a: tuple[float, float], b: tuple[float, float]) -> float:
    return sqrt((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2)


class Game(window.Window):  # type: ignore[misc]
    def __init__(self) -> None:
        super(Game, self).__init__(1200, 600, caption="WELCOME 2 GFOLF")
        self.world = World()
        self.cam = Camera(1200, 600)

        self.init_players()
        self.init_items(60, 3)

        self.controller = GameController()
        self.start_aiming()

    def init_players(self) -> None:
        sep = (randrange(10, 15), randrange(10, 15))
        o = (600, 660)

        self.players: list[Player] = []
        self.double_turn = False
        self.players.append(
            Player(pos=(o[0] + sep[0], o[1] + sep[1]), color=PlayerColor.CYAN)
        )
        self.players.append(
            Player(pos=(o[0] - sep[0], o[1] - sep[1]), color=PlayerColor.ORANGE)
        )

    def init_items(self, density: int, quantity: int) -> None:
        o = (600, 660)

        locations = [
            (o[0] + randrange(-density, density), o[1] + randrange(-density, density))
            for _ in range(quantity)
        ]
        self.items: list["Pickup"] = [Stapler(pos=x) for x in locations]

    def on_draw(self) -> None:
        self.cam.routine(self.world, self.players[0], self.players[1:], self.items)

        Blinker.try_draw()

    def on_key_press(self, symbol: int, modifiers: int) -> None:
        self.controller.press(symbol)

    def on_key_release(self, symbol: int, modifiers: int) -> None:
        self.controller.release(symbol)

    def on_resize(self, width: int, height: int) -> None:
        self.cam.refresh(width, height)

    def run(self) -> None:
        clock.schedule_interval(self.update, _FRAME_RATE)
        Blinker.start_message("GFOLF", 5)
        app.run()

    def update(self, dt: float) -> None:
        self.controller.fire_all()
        self.get_main_player().update(dt, self.players[1:])
        for i in self.items:
            i.check_collision(self.get_main_player())

    def add_player(self, player: Player) -> None:
        self.players.append(player)

    def get_main_player(self) -> Player:
        return self.players[0]

    def start_aiming(self, dt: float = 0) -> None:
        self.get_main_player().start_aiming()
        self.controller.hook_function(Button.MENU, self.close)
        self.controller.hook_function(
            Button.LEFT, lambda: self.cam.move((0, 0, _MOVE_SPEED))
        )
        self.controller.hook_function(
            Button.RIGHT, lambda: self.cam.move((0, 0, -_MOVE_SPEED))
        )
        self.controller.hook_function(Button.UP, lambda: self.cam.move((0, 1, 0)))
        self.controller.hook_function(Button.DOWN, lambda: self.cam.move((0, -1, 0)))
        self.controller.hook_function(Button.ACTION, self.start_adjusting)

    def start_adjusting(self) -> None:
        self.get_main_player().start_adjusting(self.cam.rpy[2])
        self.controller.hook_function(Button.LEFT, None)
        self.controller.hook_function(Button.RIGHT, None)
        self.controller.hook_function(Button.UP, None)
        self.controller.hook_function(Button.DOWN, None)
        self.controller.hook_function(Button.ACTION, self.start_powering)

    def start_powering(self) -> None:
        self.get_main_player().start_powering()
        self.controller.hook_function(Button.ACTION, self.start_shot)

    def start_shot(self) -> None:
        self.get_main_player().start_shot()
        self.controller.hook_function(Button.ACTION, None)
        clock.schedule_once(self.watch_shot, 2)

    def watch_shot(self, dt: float = 0) -> None:
        self.get_main_player().watch_shot()
        clock.schedule_once(self.end_shot, Shot.TRAVEL_SECONDS)

    def end_shot(self, dt: float = 0) -> None:
        pos = self.get_main_player().pos
        on_land = self.world.on_land(pos[0], pos[1])
        self.get_main_player().end_turn(on_land)
        clock.schedule_once(self.next_turn, 1.5)

    def next_turn(self, dt: float = 0) -> None:
        self.purge_dead()

        if len(self.players) > 1:
            self.get_main_player().start_aiming()
            if not self.double_turn:
                self.players = self.players[1:] + [self.players[0]]
            else:
                self.double_turn = False
            color = Blinker._PALETTE[self.get_main_player().color.value]
            Blinker.start_message("OK GO", 2, color)
            clock.schedule_once(self.start_aiming, 2)
        else:
            self.post_game()

    def post_game(self) -> None:
        color = Blinker._PALETTE[self.get_main_player().color.value]
        Blinker.start_message("YOU DID IT", 10, color)
        clock.schedule(lambda dt: self.cam.move((0, -0.1, -0.4)))

    def purge_dead(self) -> None:
        self.players = [p for p in self.players if not p.is_dead()]

        for i in [i for i in self.items if not i.active]:
            i.despawn(self)
        self.items = [i for i in self.items if i.active]
