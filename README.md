# GFOLF
_______

*"If GFOLF did not exist, it would be necessary to create it."*
                                                 *--- Voltaire*

*The year is 3002015 CE. The avarice of MAN in an age long past
caused the seas to consume the Earth. Now, the TURBO-ILLUMINATI
control the full wealth of the planet, ruling from their secret
compound on GFOLFZONE EVEREST-ONE, the only habitable land left
in the world. In the future, there is no LAW but the LAW of the
BALL, and the warrior-poets known as GFOLFers enforce their LAW
with a FIVE-IRON FIST. In the future, the word "SURVIVAL" means
TO WIN. It is a future you feared, but a future you've chosen.*

***IN THE FUTURE... THERE IS ONLY GFOLF.***

_______________________________________________________________

#### Instructions:

A game for two players. Chose your GFOLFer, then make your lil'
bro pick the other one. Hit the other guy with your GFOLF ball.
Don't get hit. Get a stapler to take an extra turn *(cheeky!)*.
Arrow keys to look around, space to shoot. Escape to rage-quit.

_______________________________________________________________

#### Installation:

GFOLF is *too advanced* to run on old, janky versions of Python
so install Python 3.11 or newer from your package manager, guy!

```bash
$ git clone https://gitlab.com/krampus/gfolf.git
$ cd gfolf
$ pip install --user .
$ gfolf
```

_______________________________________________________________

#### Credits:

I done this for THE GAME JAM // FALL 2015 @ NMT over the course
of fourty-eight hours. The theme was *TINY WORLD*, with a bonus
theme of *DEATH BY OFFICE SUPPLIES*. Then I forgot to upload it
for a year. Ops. Me bad.

All the grafix are licensed via wholesale theft. I took sprites
from *Arnold Palmer Tournament Golf* for the SNES and recolored
them to make my original characters (do not steal).  I honestly
cannot remember where I got the background vista but I think it
is pretty neat, don't you agree? Anyway. This isn't against the
rules of THE GAME JAM because there is no rules. And no brakes.
